//Name: Steven Specht, John Herring
//File: ProjectOverview.js
//Date: 29/03/2019
//Purpose: view sprint tasks, update tasks, complete sprint

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
const URL = require("../url");

class ProjectOverview extends React.PureComponent {
  state = {
    selectedProject: "",
    selectedSprint: "",
    tasks: [],
    showAssignSprint: [],
    selectedNewSprint: [],
    sprints: [],
    taskRows: "",
    snackMsg: ""
  };

  componentDidMount() {
    this.setState(
      {
        selectedSprint: this.props.selectedSprint,
        selectedProject: this.props.selectedProject,
        tasks: this.props.tasks

        // tasks: [
        //   {
        //     taskName: "GUI",
        //     estimatePoints: 6,
        //     finalPoints: undefined,
        //     pointsLeft: undefined
        //   },
        //   {
        //     taskName: "Routes",
        //     estimatePoints: 5,
        //     finalPoints: undefined,
        //     pointsLeft: undefined
        //   },
        //   {
        //     taskName: "Testing",
        //     estimatePoints: 3,
        //     finalPoints: undefined,
        //     pointsLeft: undefined
        //   }
        // ]
      },
      () => this.getOpenSprints()
    );
  }

  getOpenSprints = async () => {
    try {
      let selectedProjectId = this.state.selectedProject.id;
      let response = await fetch(
        `${URL.SERVER}finline/getSprintsOpen/` +
        selectedProjectId
      );
      let json = await response.json();

      this.setState(
        {
          sprints: json.filter(
            sprint => sprint.name !== this.state.selectedSprint.name
          )
        },
        () => this.setArry()
      );
    } catch (err) {
      console.log(err.message);
    }
  };

  setArry() {
    let temp = [];
    let tempNewSprint = [];
    //let sprint = this.state.
    this.state.tasks.forEach(element => {
      temp.push(false);
      tempNewSprint.push("");
    });

    this.setState({ showAssignSprint: temp, selectedNewSprint: tempNewSprint });
  }

  onReportBackClick = async () => {
    this.props.parentBackClick();
  };

  handleFinalPointsInput = (e, index) => {
    let tempTasks = this.state.tasks;
    let tempTask = tempTasks[index];
    tempTask.finalPoints = e.target.value;
    tempTasks[index] = tempTask;
    this.setState({ tasks: tempTasks }, this.forceUpdate());
  };

  handlePointsLeftInput = (e, index) => {
    let tempTasks = this.state.tasks;
    let tempTask = tempTasks[index];
    tempTask.pointsLeft = e.target.value;
    tempTasks[index] = tempTask;

    this.setState({ tasks: tempTasks }, () => this.checkAssignNew(index));
  };

  checkAssignNew = index => {
    if (
      this.state.tasks[index].pointsLeft !== undefined ||
      this.state.tasks[index].pointsLeft !== ""
    ) {
      let tempShowBools = this.state.showAssignSprint;
      let tempShow = tempShowBools[index];
      tempShow = true;
      tempShowBools[index] = tempShow;
      this.setState({ showAssignSprint: tempShowBools }, () =>
        this.forceUpdate()
      );
    } else {
      let tempShowBools = this.state.showAssignSprint;
      let tempShow = tempShowBools[index];
      tempShow = false;
      tempShowBools[index] = tempShow;
      this.setState({ showAssignSprint: tempShowBools }, () =>
        this.forceUpdate()
      );
    }
  };

  updateTask = async newTask => {
    try {
      let myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      let bodyStr = await JSON.stringify(newTask);
      await fetch(
        `${URL.SERVER}finline/taskUpdate`,
        {
          method: "PUT",
          headers: myHeaders,
          body: bodyStr
        }
      );
      //let json = await response.json();
      //console.log(json.msg);
    } catch (err) {
      console.log(err.message);
    } finally {
      let updateSprint = this.state.selectedSprint;
      updateSprint.status = "CLOSED";
      this.closeSprint(updateSprint);
    }
  };

  closeSprint = async updateSprint => {
    try {
      let myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      let bodyStr = await JSON.stringify(updateSprint);
      let response = await fetch(
        `${URL.SERVER}finline/sprintUpdate`,
        {
          method: "PUT",
          headers: myHeaders,
          body: bodyStr
        }
      );
      let json = await response.json();

      this.setState({ snackMsg: json.msg });
    } catch (err) {
      console.log(err.message);
    } finally {
      this.props.snackMsg(this.state.snackMsg);
    }
  };

  onSubmitClick = () => {
    //this.submissionLoop();
    this.state.tasks.forEach(task => {
      if (task.pointsLeft !== "") {
        //let pointsLeft = parseInt(task.pointsLeft);
        //let taskPointsOrg = parseInt(task.taskPoints);
        //taskPointsOrg += pointsLeft;

        // task.taskPoints = taskPointsOrg.toString();

        this.updateTask(task);
      } else {
        task.status = "CLOSED";
        this.updateTask(task);
      }
    });

    //console.log(this.state.tasks);
  };

  renderSprintDrop() {
    return this.state.sprints.map((sprint, i) => {
      return (
        <MenuItem key={i} value={sprint}>
          {sprint.name}
        </MenuItem>
      );
    });
  }

  handleSprintSelect = (e, index) => {
    let tempNewSprint = this.state.selectedNewSprint;
    tempNewSprint[index] = e.target.value;
    let taskCopy = this.state.tasks;
    taskCopy[index].sprintId = e.target.value.id;
    this.setState({ selectedNewSprint: tempNewSprint, tasks: taskCopy }, () =>
      this.forceUpdate()
    );
  };

  render() {
    const { selectedSprint, tasks, showAssignSprint } = this.state;

    const title = `${selectedSprint.name} - Report`;

    let tasksRows = tasks.map((task, taskIndex) => {
      return (
        <Card style={{ marginTop: "5%" }} key={taskIndex}>
          <CardHeader title={task.taskName} />
          <CardContent style={{ marginTop: -30 }}>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Estimated Story Points"
                      className="textField"
                      value={"Eestimated story points: " + task.taskPoints}
                      disabled={true}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Final Story Points"
                      className="textField"
                      value={task.finalPoints}
                      onChange={event =>
                        this.handleFinalPointsInput(event, taskIndex)
                      }
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Story Points To Go"
                      className="textField"
                      value={task.pointsLeft}
                      onChange={event =>
                        this.handlePointsLeftInput(event, taskIndex)
                      }
                    />
                  </TableCell>
                </TableRow>

                {showAssignSprint[taskIndex] && (
                  <TableRow>
                    <TableCell style={{ border: "none" }}>
                      <FormControl>
                        <InputLabel htmlFor="Sprint-Placeholder">
                          Assign to new sprint
                        </InputLabel>
                        <Select
                          value={this.state.selectedNewSprint[taskIndex]}
                          onChange={event =>
                            this.handleSprintSelect(event, taskIndex)
                          }
                          inputProps={{
                            name: "Sprint",
                            id: "Sprint-Placeholder"
                          }}
                          style={{ width: 200 }}
                        >
                          {this.renderSprintDrop()}
                        </Select>
                      </FormControl>
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      );
    });

    return (
      <MuiThemeProvider theme={theme}>
        <Button
          variant="contained"
          color="secondary"
          style={{ marginTop: 20 }}
          onClick={this.onReportBackClick}
        >
          Back
        </Button>
        <Card style={{ marginTop: "5%" }}>
          <CardHeader title={title} style={{ textAlign: "center" }} />
        </Card>
        {tasksRows}
        <Card style={{ marginTop: "5%", textAlign: "center" }}>
          <CardContent>
            <Button
              variant="contained"
              color="secondary"
              style={{ marginTop: 10 }}
              onClick={this.onSubmitClick}
            >
              Submit Sprint
            </Button>
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default ProjectOverview;
