//Name: Steven Specht, John Herring
//File: TaskManager.js
//Date:29/03/2019
//Purpose: add task to backlog

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableRow,
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
import PartialComplete from "./partialComplete";
import TaskSummary from "./taskSummary";
const URL = require("../url");

class Dashboard extends React.PureComponent {
  state = {
    currentproject: "",
    sprints: [],
    tasks: [],
    velocities: []
  };

  componentDidMount() {
    this.setState(
      {
        currentproject: this.props.selectedProject,
        sprints: []
      },
      () => this.getClosedSprints()
    );
  }

  getClosedSprints = async () => {
    try {
      let selectedProjectId = this.props.selectedProject.id;
      let response = await fetch(
        `${URL.SERVER}finline/getSprintsClosed/` +
        selectedProjectId
      );
      let json = await response.json();

      this.setState({ sprints: json });
    } catch (err) {
      console.log(err.message);
    } finally {
      this.getAallTasks();
    }
  };

  getAallTasks = async () => {
    this.state.sprints.forEach(sprint => {
      this.getTasks(sprint.id);
    });
  };

  getTasks = async id => {
    try {
      let tempTasks = this.state.tasks;
      //let x = this.state.selectedSprint.id;
      let response = await fetch(
        `${URL.SERVER}finline/getTasksBySprint/` +
        id
      );
      let json = await response.json();
      tempTasks = tempTasks.concat(json);
      this.setState({ tasks: tempTasks });
    } catch (err) {
      console.log(err.message);
    } finally {
      this.calculateVelocity(id);
    }
  };

  calculateVelocity = async sprintId => {
    let sprintTasks = this.state.tasks.filter(x => x.sprintId === sprintId);
    let totalActual = 0;

    sprintTasks.forEach(task => {
      totalActual += parseInt(task.finalPoints);
    });
    let tempVelocity = this.state.velocities;
    let sprintVelocity = {
      id: sprintId,
      velocity: totalActual
    };
    tempVelocity.push(sprintVelocity);
    this.setState({ velocities: tempVelocity }, () => this.forceUpdate());
  };

  onBackClick = async () => {
    this.props.parentBackClick();
  };

  render() {
    const { currentproject, sprints, velocities } = this.state;

    const title = `${currentproject.projectName} - Dashboard`;

    let sprintRows = sprints.map((sprint, sprintndex) => {
      return (
        <TableRow key={sprintndex}>
          <TableCell>{sprint.name}</TableCell>
          <TableCell>
            {velocities &&
              velocities.length &&
              velocities.find(x => x.id === sprint.id)
              ? velocities.find(x => x.id === sprint.id).velocity
              : ""}
          </TableCell>
        </TableRow>
      );
    });

    return (
      <MuiThemeProvider theme={theme}>
        <Button
          variant="contained"
          color="secondary"
          style={{ marginTop: 20 }}
          onClick={this.onBackClick}
        >
          Back
        </Button>
        <Card style={{ marginTop: "5%" }}>
          <CardHeader title={title} style={{ textAlign: "center" }} />
          <CardContent>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Sprint</TableCell>
                  <TableCell>Velocity</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>{sprintRows}</TableBody>
            </Table>
          </CardContent>
        </Card>
        <PartialComplete currentproject={currentproject} />
        <TaskSummary currentproject={currentproject} />
      </MuiThemeProvider>
    );
  }
}
export default Dashboard;
