//Name: Steven Specht, John Herring
//File: ProjectOverview.js
//Date: 29/03/2019
//Purpose: view sprint tasks, update tasks, complete sprint

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  FormControl,
  InputLabel,
  MenuItem,
  Table,
  TableBody,
  TableRow,
  TableCell,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import UpdateTask from "./UpdateTask";
import SprintReport from "./SprintReport";
const URL = require("../url");

class ProjectOverview extends React.PureComponent {
  state = {
    sprints: [],
    selectedSprint: "",
    currentproject: "",
    tasks: [],
    selectedTaskIndex: 0,
    showOverview: true,
    showUpdateDialog: false,
    showSprintReport: false
  };

  componentDidMount = async () => {
    this.setState({
      currentproject: this.props.selectedProject
    });

    await this.getSprints();

    this.renderSprintDrop();
  };

  getSprints = async () => {
    try {
      let selectedProjectId = this.props.selectedProject.id;
      let response = await fetch(
        `${URL.SERVER}finline/getSprints/` + selectedProjectId
      );
      let json = await response.json();
      this.setState({ sprints: json });
    } catch (err) {
      console.log(err.message);
    }
  };

  getTasks = async id => {
    try {
      //let x = this.state.selectedSprint.id;
      let response = await fetch(
        `${URL.SERVER}finline/getTasksBySprint/` +
        id
      );
      let json = await response.json();
      this.setState({ tasks: json });
    } catch (err) {
      console.log(err.message);
    }
  };

  handleSprintSelect = async e => {
    this.setState({ selectedSprint: e.target.value });
    await this.getTasks(e.target.value.id);
    this.renderTaskList();
  };

  renderSprintDrop() {
    return this.state.sprints.map((sprint, i) => {
      return (
        <MenuItem key={i} value={sprint}>
          {sprint.name}
        </MenuItem>
      );
    });
  }

  renderTaskList() {
    return this.state.tasks.map((task, i) => {
      return <ListItem key={i} ListItemText={task.taskName} />;
    });
  }

  onBackClick = async () => {
    this.props.parentBackClick();
  };

  onReportBackClick = async () => {
    this.setState({
      showOverview: true,
      showUpdateDialog: false,
      showSprintReport: false
    });
  };

  handleTaskListClick = (event, index) => {
    //console.log(this.state.showUpdateDialog);
    this.setState({ selectedTaskIndex: index, showUpdateDialog: true });
  };

  onCompleteClick = async () => {
    this.setState({ showSprintReport: true, showOverview: false });
  };

  handleVisbile = setVisible => {
    this.setState({ showUpdateDialog: setVisible });
  };

  render() {
    const {
      selectedSprint,
      selectedTaskIndex,
      currentproject,
      tasks,
      showOverview,
      showUpdateDialog,
      showSprintReport
    } = this.state;

    const title = `${currentproject.projectName} - Overview`;

    let completeValidation =
      selectedSprint === undefined || selectedSprint === "";

    let taskRows = tasks.map((task, taskIndex) => {
      return (
        <ListItem
          button
          selected={this.state.selectedIndex === taskIndex}
          onClick={event => this.handleTaskListClick(event, taskIndex)}
          key={taskIndex}
          divider={true}
        >
          <ListItemText primary={task.taskName} />
        </ListItem>
      );
    });

    return (
      <MuiThemeProvider theme={theme}>
        {showOverview && (
          <Button
            variant="contained"
            color="secondary"
            style={{ marginTop: 20 }}
            onClick={this.onBackClick}
          >
            Back
          </Button>
        )}

        {showOverview && (
          <Card style={{ marginTop: "2%" }}>
            <CardHeader title={title} style={{ textAlign: "center" }} />
            <CardContent>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell style={{ border: "none", textAlign: "center" }}>
                      <FormControl>
                        <InputLabel htmlFor="Sprint-Placeholder">
                          Sprint
                        </InputLabel>
                        <Select
                          value={selectedSprint}
                          onChange={this.handleSprintSelect}
                          inputProps={{
                            name: "Sprint",
                            id: "Sprint-Placeholder"
                          }}
                          style={{ width: 200 }}
                        >
                          {this.renderSprintDrop()}
                        </Select>
                      </FormControl>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </CardContent>
          </Card>
        )}

        {showOverview && selectedSprint !== undefined && selectedSprint !== "" && (
          <Card style={{ marginTop: "5%" }}>
            <CardHeader title="Task & Status" style={{ textAlign: "center" }} />
            <CardContent style={{ marginTop: "1%" }}>
              <List component="nav">{taskRows}</List>
            </CardContent>
          </Card>
        )}

        {showOverview && (
          <Card style={{ marginTop: "5%", textAlign: "center" }}>
            <CardContent>
              <Button
                variant="contained"
                color="secondary"
                style={{ marginTop: 10 }}
                onClick={this.onCompleteClick}
                disabled={completeValidation}
              >
                Complete Sprint
              </Button>
            </CardContent>
          </Card>
        )}

        {showUpdateDialog && !showSprintReport && (
          <UpdateTask
            Visibility={showUpdateDialog}
            childData={someDataFromChild =>
              this.handleVisbile(someDataFromChild)
            }
            snackMsg={this.props.snackMsg}
            task={tasks[selectedTaskIndex]}
            owners={currentproject.users}
          />
        )}
        {showSprintReport && (
          <SprintReport
            selectedSprint={selectedSprint}
            selectedProject={currentproject}
            tasks={tasks}
            parentBackClick={this.onReportBackClick}
            snackMsg={this.props.snackMsg}
          />
        )}
      </MuiThemeProvider>
    );
  }
}
export default ProjectOverview;
