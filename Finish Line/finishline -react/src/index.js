import React from "react";
import ReactDOM from "react-dom";
import Root from "./Pages/ProjectManager";
ReactDOM.render(<Root />, document.getElementById("root"));
