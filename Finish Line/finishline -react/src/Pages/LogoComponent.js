//Name: Steven Specht, John Herring
//File: LogoComponent.js
//Date: 29/03/2019
//Purpose: log fof the app

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Typography, Card, CardHeader } from "@material-ui/core";
import theme from "./theme";
import "../App.css";

class LogoComponent extends React.PureComponent {
  state = {
    height: "",
    width: "",
    showText: true
  };

  componentDidMount() {
    this.setState({
      height: this.props.height,
      width: this.props.width,
      showText: this.props.showText
    });
  }

  render() {
    // use the state variables locally
    const { height, width, showText } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <Typography
          variant="h5"
          color="inherit"
          style={{
            textAlign: "center",
            marginTop: "4vh"
          }}
        >
          <center>
            <img
              src={window.location.origin + "/images/flaglogo.png"}
              alt="flaglogo"
              height={height}
              width={width}
            />
          </center>
        </Typography>

        {showText && (
          <Card style={{ marginTop: "5%" }}>
            <CardHeader
              title="Finish Line - A Sprint Planner"
              style={{ textAlign: "center" }}
            />
          </Card>
        )}
      </MuiThemeProvider>
    );
  }
}
export default LogoComponent;
