//Name: Steven Specht, John Herring
//File: TaskManager.js
//Date:29/03/2019
//Purpose: add task to backlog

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Table,
  TableBody,
  TableRow,
  TableCell
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
const URL = require("../url");

class TaskManager extends React.PureComponent {
  state = {
    taskName: "",
    taskDescription: "",
    taskPoints: "",
    taskOwner: "",
    owners: [],
    selectedOwner: "",
    currentproject: ""
  };

  componentDidMount() {
    this.setState({
      owners: this.props.selectedProject.users || [],
      currentproject: this.props.selectedProject
    });
    this.renderOwnerDrop();
  }

  handleNameInput = e => {
    this.setState({ taskName: e.target.value });
  };

  handleDescriptionInput = e => {
    this.setState({ taskDescription: e.target.value });
  };

  handlePointsInput = e => {
    this.setState({ taskPoints: e.target.value });
  };

  handleOwnerSelect = e => {
    this.setState({ selectedOwner: e.target.value });
  };

  renderOwnerDrop() {
    return this.state.owners.map((owner, i) => {
      return (
        <MenuItem key={i} value={owner}>
          {owner}
        </MenuItem>
      );
    });
  }

  onAddTaskClicked = async () => {
    let newTask = {
      taskName: this.state.taskName,
      taskDesc: this.state.taskDescription,
      taskUser: this.state.selectedOwner,
      taskPoints: this.state.taskPoints,
      projectId: this.state.currentproject.id,
      sprintId: "",
      finalPoints: "",
      pointsLeft: "",
      status: "OPEN"
    };

    await this.createNewTask(newTask);
  };

  createNewTask = async newTask => {
    let response;
    try {
      let myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      let bodyStr = await JSON.stringify(newTask);
      response = await fetch(
        `${URL.SERVER}finline/newTask`,
        {
          method: "POST",
          headers: myHeaders,
          body: bodyStr
        }
      );
      let json = await response.json();
      this.props.snackMsg(json.msg);
      this.setState({
        taskName: "",
        taskDescription: "",
        selectedOwner: "",
        taskPoints: ""
      });
    } catch (err) {
      console.log(err.message);
    }
  };

  onBackClick = async () => {
    this.props.parentBackClick();
  };

  render() {
    const {
      taskName,
      taskDescription,
      taskPoints,
      selectedOwner,
      currentproject
    } = this.state;

    const fieldsValidity =
      taskName === undefined ||
      taskName === "" ||
      taskDescription === undefined ||
      taskDescription === "" ||
      taskPoints === undefined ||
      taskPoints === "" ||
      selectedOwner === undefined ||
      selectedOwner === "";

    const title = `${currentproject.projectName} - Task Manager`;

    return (
      <MuiThemeProvider theme={theme}>
        <Button
          variant="contained"
          color="secondary"
          style={{ marginTop: 20 }}
          onClick={this.onBackClick}
        >
          Back
        </Button>
        <Card style={{ marginTop: "5%" }}>
          <CardHeader title={title} style={{ textAlign: "center" }} />
          <CardContent>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Task Name"
                      className="textField"
                      value={taskName}
                      onChange={this.handleNameInput}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Description"
                      className="textField"
                      value={taskDescription}
                      onChange={this.handleDescriptionInput}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Story Points"
                      className="textField"
                      value={taskPoints}
                      onChange={this.handlePointsInput}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <FormControl>
                      <InputLabel htmlFor="Owner-Placeholder">Owner</InputLabel>
                      <Select
                        value={selectedOwner}
                        onChange={this.handleOwnerSelect}
                        inputProps={{
                          name: "Owner",
                          id: "Owner-Placeholder"
                        }}
                        style={{ width: 200 }}
                      >
                        {this.renderOwnerDrop()}
                      </Select>
                    </FormControl>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none", textAlign: "center" }}>
                    <Button
                      variant="contained"
                      color="secondary"
                      style={{ marginTop: 20 }}
                      onClick={this.onAddTaskClicked}
                      disabled={fieldsValidity}
                    >
                      Add To Backlog
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default TaskManager;
