import { createMuiTheme } from "@material-ui/core/styles";
export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    common: { black: "#000", white: "#fff" },
    background: { paper: "#fff", default: "#fafafa" },
    primary: {
      light: "rgba(155, 34, 48, 0.64)",
      main: "rgba(155, 34, 48, 0.89)",
      dark: "rgba(155, 34, 48, 1)",
      contrastText: "#fff"
    },
    secondary: {
      light: "rgba(133, 191, 255, 1)",
      main: "rgba(75, 161, 255, 1)",
      dark: "rgba(0, 122, 255, 1)",
      contrastText: "#fff"
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff"
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  }
});
