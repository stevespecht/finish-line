const express = require("express");
const router = express.Router();
const dbRoutines = require("./dbRoutines");
var firebase = require("firebase");

// Initialize Firebase
var config = {
  apiKey: "AIzaSyBK9G9ios_aA3X6sUexjNb8yzI2ikkHT8k",
  authDomain: "finish-line-42e6f.firebaseapp.com",
  databaseURL: "https://finish-line-42e6f.firebaseio.com",
  projectId: "finish-line-42e6f",
  storageBucket: "finish-line-42e6f.appspot.com",
  messagingSenderId: "121346443865"
};
firebase.initializeApp(config);

// Get a reference to the database service
var db = firebase.firestore();

router.get("/getSprints/:projectId", async (req, res) => {
  let projectId = req.params.projectId;
  try {
    let results = await dbRoutines.findSpecific(
      db.collection("Sprints"),
      "projectId",
      projectId
    );

    results = convertToArray(results);
    results.sort((a, b) => a.name.localeCompare(b.name));
    res.send(results);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getSprintsOpen/:projectId", async (req, res) => {
  let projectId = req.params.projectId;
  try {
    let results = await dbRoutines.findSprintsOpen(
      db.collection("Sprints"),
      projectId
    );

    results = convertToArray(results);
    results.sort((a, b) => a.name.localeCompare(b.name));
    res.send(results);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getSprintsClosed/:projectId", async (req, res) => {
  let projectId = req.params.projectId;
  try {
    let results = await dbRoutines.findSprintsClosed(
      db.collection("Sprints"),
      projectId
    );

    results = convertToArray(results);
    results.sort((a, b) => a.name.localeCompare(b.name));
    res.send(results);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getTasksBySprint/:sprintId", async (req, res) => {
  let sprintId = req.params.sprintId;
  try {
    let results = await dbRoutines.findTasksBySprint(
      db.collection("tasks"),
      sprintId
    );

    results = convertToArray(results);
    results.sort((a, b) => a.taskName.localeCompare(b.taskName));
    res.send(results);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getTasks/:projectId", async (req, res) => {
  let projectId = req.params.projectId;
  try {
    let results = await dbRoutines.findTasksByProjAndSprint(
      db.collection("tasks"),
      projectId
    );

    results = convertToArray(results);
    results.sort((a, b) => a.taskName.localeCompare(b.taskName));
    res.send(results);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getTasksByProj/:projectId", async (req, res) => {
  let projectId = req.params.projectId;
  try {
    let results = await dbRoutines.findTasksByProj(
      db.collection("tasks"),
      projectId
    );

    results = convertToArray(results);
    results.sort((a, b) => a.taskName.localeCompare(b.taskName));
    res.send(results);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getUsers", async (req, res) => {
  try {
    let results = await dbRoutines.findUsers(db.collection("users"));
    res.send(convertToArray(results));
  } catch (err) {
    res.status(500).send(err);
  }
});

router.put("/projectUpdate", async (req, res) => {
  try {
    let results = await dbRoutines.updateProject(
      db.collection("Projects"),
      req.body.id,
      req.body
    );
    res.send(
      `${JSON.stringify({ msg: `${req.body.projectName} has been updated` })}`
    );
  } catch (err) {
    res.status(500).send(err);
  }
});

router.put("/taskUpdate", async (req, res) => {
  try {
    let results = await dbRoutines.updateProject(
      db.collection("tasks"),
      req.body.id,
      req.body
    );
    let resMsg = `${req.body.taskName} has been updated`;
    res.send(`${JSON.stringify({ msg: resMsg })}`);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.put("/sprintUpdate", async (req, res) => {
  try {
    let results = await dbRoutines.updateProject(
      db.collection("Sprints"),
      req.body.id,
      req.body
    );
    let resMsg = `${req.body.name} has been closed`;
    res.send(`${JSON.stringify({ msg: resMsg })}`);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.post("/newSprint", async (req, res) => {
  try {
    insertData = req.body;

    let result = await dbRoutines.insertData(
      db.collection("Sprints"),
      insertData
    );
    let resMsg = `${req.body.name} added to backlog of project`;
    res.send(`${JSON.stringify({ msg: resMsg })}`);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.post("/newTask", async (req, res) => {
  try {
    insertData = req.body;

    let result = await dbRoutines.insertData(
      db.collection("tasks"),
      insertData
    );
    let resMsg = `Task added to backlog of project`;
    res.send(`${JSON.stringify({ msg: resMsg })}`);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.post("/newProject", async (req, res) => {
  try {
    insertData = req.body;

    let result = await dbRoutines.insertData(
      db.collection("Projects"),
      insertData
    );
    let resMsg = `${insertData.projectName} has been created`;
    res.send(`${JSON.stringify({ msg: resMsg })}`);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getSprints", async (req, res) => {
  try {
    let results = await dbRoutines.findUsers(db.collection("Sprints"));
    results = convertToArray(results);
    results.sort((a, b) => a.name.localeCompare(b.name));
    res.send(results);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getProjects", async (req, res) => {
  try {
    let results = await dbRoutines.findUsers(db.collection("Projects"));
    results = convertToArray(results);
    results.sort((a, b) => a.projectName.localeCompare(b.projectName));
    res.send(results);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getTasks", async (req, res) => {
  try {
    let results = await dbRoutines.findUsers(db.collection("tasks"));
    results = convertToArray(results);
    results.sort((a, b) => a.taskName.localeCompare(b.taskName));
    res.send(results);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get("/getUsers/:name", async (req, res) => {
  let name = req.params.name;
  try {
    let results = await dbRoutines.findSpecific(
      db.collection("users"),
      "name",
      name
    );
    //console.log(convertToArray(results));
    res.send(convertToArray(results));
  } catch (err) {
    res.status(500).send(err);
  }
});

router.post("/newuser", async (req, res) => {
  try {
    insertData = req.body;
    let result = await dbRoutines.insertData(
      db.collection("users"),
      insertData
    );
    console.log(result.id);
    res.send(result.id);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.delete("/user/:docid", async (req, res) => {
  try {
    let result = await dbRoutines.deleteDoc(
      db.collection("users"),
      req.params.docid
    );
    //console.log(result);
    res.send({ success: 1, message: "User deleted" });
  } catch (err) {
    res.status(500).send(err);
  }
});

function convertToArray(results) {
  let dataArr = [];
  results.forEach(doc => {
    let temp = doc.data();
    temp.id = doc.id;
    dataArr.push(temp);
  });
  return dataArr;
}

module.exports = router;
