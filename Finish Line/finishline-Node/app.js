const myroutes = require("./routes/finLineRoutes");
const env = require("dotenv").config({ path: `${__dirname}/.env` });
const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const app = express();
app.use(express.static("public"));
let server = http.createServer(app);

const port = process.env.PORT || 5000;
//cors;
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
  next();
});

// parse application/json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/finline", myroutes);

app.use((req, res, next) => {
  console.log("Time:", new Date() + 3600000 * -5.0);
  // GMT-->EST
  next();
});

//app.use(express.static("public"));
app.use((err, req, res, next) => {
  // Do logging and user-friendly error message display
  console.error(err);
  res.status(500).send("internal server error");
});

// app.listen(port, () => {
//   console.log(`listening on port ${port} - ${process.env.NODE_ENV}`);
// });

server.listen(port, () => console.log(`starting on port ${port}`));
