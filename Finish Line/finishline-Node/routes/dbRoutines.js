const findUsers = dbCollection => dbCollection.get();

const findSpecific = (dbCollection, param, value) =>
  dbCollection.where(param, `==`, value).get();

findSprintsOpen = (dbCollection, projectId) =>
  dbCollection
    .where(`projectId`, `==`, projectId)
    .where(`status`, `==`, `OPEN`)
    .get();

findSprintsClosed = (dbCollection, projectId) =>
  dbCollection
    .where(`projectId`, `==`, projectId)
    .where(`status`, `==`, `CLOSED`)
    .get();

const findTasksByProjAndSprint = (dbCollection, projectValue) =>
  dbCollection
    .where(`projectId`, `==`, projectValue)
    .where(`sprintId`, `==`, "")
    .get();

const findTasksByProj = (dbCollection, projectId) =>
  dbCollection.where(`projectId`, `==`, projectId).get();

const findTasksBySprint = (dbCollection, sprintValue) =>
  dbCollection.where(`sprintId`, `==`, sprintValue).get();

const insertData = (dbCollection, insertData) => dbCollection.add(insertData);

const deleteDoc = (dbCollection, docID) => dbCollection.doc(docID).delete();

const updateProject = (dbCollection, docID, updateData) =>
  dbCollection.doc(docID).set(updateData, { merge: true });

module.exports = {
  findUsers,
  findSpecific,
  findSprintsClosed,
  insertData,
  deleteDoc,
  updateProject,
  findTasksByProjAndSprint,
  findTasksByProj,
  findTasksBySprint,
  findSprintsOpen
};
