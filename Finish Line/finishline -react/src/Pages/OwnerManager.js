//Name: Steven Specht, John Herring
//File: OwnerManager.js
//Date: 29/03/2019
//Purpose: add owner to project

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Table,
  TableBody,
  TableRow,
  TableCell,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
const URL = require("../url");

class OwnerManager extends React.PureComponent {
  state = {
    currentproject: "",
    owners: [],
    newOwner: ""
  };

  componentDidMount() {
    this.setState({
      currentproject: this.props.selectedProject,
      owners: this.props.selectedProject.users || []
    });
  }

  handleNewOwnerInput = e => {
    this.setState({ newOwner: e.target.value });
  };

  updateProjects = async () => {
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    let bodyStr = await JSON.stringify(this.state.currentproject);
    let response = await fetch(
      `${URL.SERVER}finline/projectUpdate`,
      {
        method: "PUT",
        headers: myHeaders,
        body: bodyStr
      }
    );
    let json = await response.json();
    this.props.snackMsg(json.msg);
  };

  onAddClicked = async () => {
    this.state.owners.push(this.state.newOwner);
    let copyProject = this.state.currentproject;
    copyProject.users = this.state.owners;
    this.setState(
      {
        currentproject: copyProject,
        newOwner: ""
      },
      await this.updateProjects()
    );
    //console.log(`Name: ${this.state.newOwner}`);
  };

  onBackClick = async () => {
    this.props.parentBackClick();
  };

  render() {
    const { currentproject, owners, newOwner } = this.state;

    const title = `${currentproject.projectName} - Owner Manager`;

    let ownerRows = owners.map((owner, ownerIndex) => {
      return (
        <ListItem key={ownerIndex} divider={true}>
          <ListItemText primary={owner} />
        </ListItem>
      );
    });

    const ownerValidation = newOwner === undefined || newOwner === "";

    return (
      <MuiThemeProvider theme={theme}>
        <Button
          variant="contained"
          color="secondary"
          style={{ marginTop: 20 }}
          onClick={this.onBackClick}
        >
          Back
        </Button>

        <Card style={{ marginTop: "5%" }}>
          <CardHeader title={title} style={{ textAlign: "center" }} />
        </Card>

        <Card style={{ marginTop: "5%" }}>
          <CardHeader title="Current Owners" style={{ textAlign: "center", paddingBottom: "0px" }} />
          <CardContent>
            <List component="nav">{ownerRows}</List>
          </CardContent>
        </Card>

        <Card style={{ marginTop: "5%" }}>
          <CardHeader title="Add Owner" style={{ textAlign: "center" }} />
          <CardContent>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Owner Name"
                      className="textField"
                      value={newOwner}
                      onChange={this.handleNewOwnerInput}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none", textAlign: "center" }}>
                    <Button
                      variant="contained"
                      color="secondary"
                      style={{ marginTop: 20 }}
                      onClick={this.onAddClicked}
                      disabled={ownerValidation}
                    >
                      Add
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default OwnerManager;
