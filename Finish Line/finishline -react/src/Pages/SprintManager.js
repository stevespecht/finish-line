//Name: Steven Specht, John Herring
//File: SprintManager.js
//Date: 29/03/2019
//Purpose: add sprint to project, add task to sprint

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Table,
  TableBody,
  TableRow,
  TableCell
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
const URL = require("../url");

class TaskManager extends React.PureComponent {
  state = {
    sprintName: "",
    sprints: [],
    selectedSprint: "",
    tasks: [],
    selectedTask: "",
    currentproject: ""
  };

  componentDidMount = async () => {
    this.setState({
      sprints: [],
      tasks: [],
      currentproject: this.props.selectedProject
    });

    await this.getSprints();
    await this.getTasks();

    this.renderSprintDrop();
    this.renderTaskDrop();
  };

  getSprints = async () => {
    try {
      let selectedProjectId = this.props.selectedProject.id;
      let response = await fetch(
        `${URL.SERVER}finline/getSprintsOpen/` +
        selectedProjectId
      );
      let json = await response.json();

      this.setState({
        sprints: json
      });
    } catch (err) {
      console.log(err.message);
    }
  };

  getTasks = async () => {
    try {
      let selectedProjectId = this.props.selectedProject.id;
      let response = await fetch(
        `${URL.SERVER}finline/getTasks/` + selectedProjectId
      );
      let json = await response.json();
      this.setState({ tasks: json });
    } catch (err) {
      console.log(err.message);
    }
  };

  handleSprintInput = e => {
    this.setState({ sprintName: e.target.value });
  };

  handleSprintSelect = e => {
    this.setState({ selectedSprint: e.target.value });
  };

  handleTaskSelect = e => {
    this.setState({ selectedTask: e.target.value });
  };

  renderSprintDrop() {
    return this.state.sprints.map((sprint, i) => {
      return (
        <MenuItem key={i} value={sprint}>
          {sprint.name}
        </MenuItem>
      );
    });
  }

  renderTaskDrop() {
    return this.state.tasks.map((task, i) => {
      return (
        <MenuItem key={i} value={task}>
          {task.taskName}
        </MenuItem>
      );
    });
  }

  onAddSprintClicked = async () => {
    let newSprint = {
      name: this.state.sprintName,
      projectId: this.state.currentproject.id,
      status: "OPEN"
    };
    await this.createNewSprint(newSprint);
    await this.getSprints();
    //console.log(`New Sprint: ${this.state.sprintName} `);
    this.setState({ sprintName: "" });
  };

  createNewSprint = async newSprint => {
    try {
      let myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      let bodyStr = JSON.stringify(newSprint);
      let response = await fetch(
        `${URL.SERVER}finline/newSprint`,
        {
          method: "POST",
          headers: myHeaders,
          body: bodyStr
        }
      );
      let json = await response.json();
      this.props.snackMsg(json.msg);
    } catch (err) {
      console.log(err.message);
    }
  };

  updateTask = async newTask => {
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    let bodyStr = await JSON.stringify(newTask);
    await fetch(
      `${URL.SERVER}finline/taskUpdate`,
      {
        method: "PUT",
        headers: myHeaders,
        body: bodyStr
      }
    );
  };

  onAddTaskToSprintClicked = async () => {
    let newTask = this.state.selectedTask;
    newTask.sprintId = this.state.selectedSprint.id;
    await this.updateTask(newTask);
    await this.getTasks();
    this.setState({ selectedTask: "", selectedSprint: "" });
    // console.log(
    //   `Add task ${this.state.selectedTask} to ${this.state.selectedSprint}`
    // );
  };

  onBackClick = async () => {
    this.props.parentBackClick();
  };

  render() {
    const {
      sprintName,
      selectedSprint,
      selectedTask,
      currentproject
    } = this.state;

    const sprintCardValidity = sprintName === undefined || sprintName === "";
    const taskCardValidity =
      selectedSprint === undefined ||
      selectedSprint === "" ||
      selectedTask === undefined ||
      selectedTask === "";

    const title = `${currentproject.projectName} - Sprint Manager`;

    return (
      <MuiThemeProvider theme={theme}>
        <Button
          variant="contained"
          color="secondary"
          style={{ marginTop: 20 }}
          onClick={this.onBackClick}
        >
          Back
        </Button>
        <Card style={{ marginTop: "5%" }}>
          <CardHeader title={title} style={{ textAlign: "center" }} />
        </Card>

        <Card style={{ marginTop: "5%" }}>
          <CardHeader
            title="Add Sprint to Project"
            style={{ textAlign: "center" }}
          />
          <CardContent>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Sprint Name"
                      className="textField"
                      value={sprintName}
                      onChange={this.handleSprintInput}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none", textAlign: "center" }}>
                    <Button
                      variant="contained"
                      color="secondary"
                      style={{ marginTop: 20 }}
                      onClick={this.onAddSprintClicked}
                      disabled={sprintCardValidity}
                    >
                      Add Sprint
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </CardContent>
        </Card>

        <Card style={{ marginTop: "5%" }}>
          <CardHeader
            title="Add Task to Sprint"
            style={{ textAlign: "center" }}
          />
          <CardContent>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <FormControl>
                      <InputLabel htmlFor="Sprint-Placeholder">
                        Sprint
                      </InputLabel>
                      <Select
                        value={selectedSprint}
                        onChange={this.handleSprintSelect}
                        inputProps={{
                          name: "Sprint",
                          id: "Sprint-Placeholder"
                        }}
                        style={{ width: 200 }}
                      >
                        {this.renderSprintDrop()}
                      </Select>
                    </FormControl>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <FormControl>
                      <InputLabel htmlFor="Task-Placeholder">Task</InputLabel>
                      <Select
                        value={selectedTask}
                        onChange={this.handleTaskSelect}
                        inputProps={{
                          name: "Task",
                          id: "Task-Placeholder"
                        }}
                        style={{ width: 200 }}
                      >
                        {this.renderTaskDrop()}
                      </Select>
                    </FormControl>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none", textAlign: "center" }}>
                    <Button
                      variant="contained"
                      color="secondary"
                      style={{ marginTop: 20 }}
                      onClick={this.onAddTaskToSprintClicked}
                      disabled={taskCardValidity}
                    >
                      Add Task to Sprint
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default TaskManager;
