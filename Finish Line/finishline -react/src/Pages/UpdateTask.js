//Name: Steven Specht
//File: addComponent.js
//Date: 06/03/2019
//Purpose: add advisory component for the travel advisory app

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
const URL = require("../url");

class UpdateTask extends React.PureComponent {
  state = {
    open: false,
    taskName: "",
    taskDescription: "",
    taskPoints: "",
    taskOwner: "",
    owners: [],
    selectedOwner: "",
    task: ""
  };

  componentDidMount() {
    //console.log(this.props.task);
    this.setState({
      task: this.props.task,
      taskName: this.props.task.taskName,
      taskDescription: this.props.task.taskDesc,
      taskPoints: this.props.task.taskPoints,
      selectedOwner: this.props.task.taskUser,
      owners: this.props.owners,
      open: this.props.Visibility
    });
    this.renderOwnerDrop();
  }

  handleNameInput = e => {
    this.setState({ taskName: e.target.value });
  };

  handleDescriptionInput = e => {
    this.setState({ taskDescription: e.target.value });
  };

  handlePointsInput = e => {
    this.setState({ taskPoints: e.target.value });
  };

  handleOwnerSelect = e => {
    this.setState({ selectedOwner: e.target.value });
  };

  renderOwnerDrop() {
    return this.state.owners.map((owner, i) => {
      return (
        <MenuItem key={i} value={owner}>
          {owner}
        </MenuItem>
      );
    });
  }

  updateTask = async newTask => {
    try {
      let myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      let bodyStr = await JSON.stringify(newTask);
      let response = await fetch(
        `${URL.SERVER}finline/taskUpdate`,
        {
          method: "PUT",
          headers: myHeaders,
          body: bodyStr
        }
      );
      let json = await response.json();
      this.props.snackMsg(json.msg);
    } catch (err) {
      console.log(err.message);
    }
  };

  onUpdateClicked = async () => {
    let newTask = this.state.task;
    newTask.taskName = this.state.taskName;
    newTask.taskDesc = this.state.taskDescription;
    newTask.taskPoints = this.state.taskPoints;
    newTask.taskUser = this.state.selectedOwner;

    this.updateTask(newTask);
    this.props.childData(false);
    // console.log(
    //   `Name: ${this.state.taskName} Description: ${this.state.taskDescription
    //   } Points: ${this.state.taskPoints} Owner: ${this.state.selectedOwner}`
    // );
  };

  onCancelClicked = async () => {
    this.props.childData(false);
    this.setState({ open: false });
  };

  render() {
    const {
      fullScreen,
      taskName,
      taskDescription,
      taskPoints,
      selectedOwner
    } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <Dialog
          fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">
            {"Update Task"}
          </DialogTitle>
          <DialogContent>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Task Name"
                      className="textField"
                      value={taskName}
                      onChange={this.handleNameInput}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Description"
                      className="textField"
                      value={taskDescription}
                      onChange={this.handleDescriptionInput}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Story Points"
                      className="textField"
                      value={taskPoints}
                      onChange={this.handlePointsInput}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <FormControl>
                      <InputLabel htmlFor="Owner-Placeholder">Owner</InputLabel>
                      <Select
                        value={selectedOwner}
                        onChange={this.handleOwnerSelect}
                        inputProps={{
                          name: "Owner",
                          id: "Owner-Placeholder"
                        }}
                        style={{ width: 200 }}
                      >
                        {this.renderOwnerDrop()}
                      </Select>
                    </FormControl>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </DialogContent>
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              onClick={this.onUpdateClicked}
            >
              Update
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={this.onCancelClicked}
            >
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </MuiThemeProvider>
    );
  }
}
export default UpdateTask;
