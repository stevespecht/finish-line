import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableRow,
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
const URL = require("../url");

class TaskSummary extends React.PureComponent {
  state = {
    currentproject: "",
    tasks: []
  };

  componentDidMount() {
    this.setState({ currentproject: this.props.currentproject }, () =>
      this.getTasks()
    );
  }

  getTasks = async () => {
    let filter = [];
    try {
      let currentProjectId = this.props.currentproject.id;
      let response = await fetch(
        `${URL.SERVER}finline/getTasksByProj/` +
        currentProjectId
      );
      let json = await response.json();
      filter = json;
    } catch (err) {
      console.log(err.message);
    } finally {
      let taskCopy = filter;
      taskCopy.map(task => {
        return (
          (task.pointsWorked = task.finalPoints === "" ? 0 : task.finalPoints),
          (task.restimate = task.pointsLeft ? task.pointsLeft : 0)
        );
      });

      this.setState({ tasks: taskCopy }, () => this.forceUpdate());
    }
  };

  render() {
    const { tasks } = this.state;
    let taskEst = tasks.length ? (
      tasks.map((task, taskindex) => {
        return (
          <TableRow key={taskindex}>
            <TableCell>{task.taskName}</TableCell>
            <TableCell> {task.status}</TableCell>
            <TableCell> {task.pointsWorked}</TableCell>
            <TableCell> {task.restimate}</TableCell>
          </TableRow>
        );
      })
    ) : (
        <TableRow />
      );

    const title = "Task Summary for " + this.props.currentproject.projectName;
    return (
      <MuiThemeProvider theme={theme}>
        <Card style={{ marginTop: "5%" }}>
          <CardHeader title={title} style={{ textAlign: "center" }} />
          <CardContent>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Task Name</TableCell>
                  <TableCell>Task Status</TableCell>
                  <TableCell>Points Worked</TableCell>
                  <TableCell>Re-estimate Points</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>{taskEst}</TableBody>
            </Table>
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default TaskSummary;
