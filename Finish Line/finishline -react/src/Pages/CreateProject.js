//Name: Steven Specht, John Herring
//File: TaskManager.js
//Date:29/03/2019
//Purpose: add task to backlog

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Table,
  TableBody,
  TableRow,
  TableCell
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
const URL = require("../url");

class CreateProject extends React.PureComponent {
  state = {
    projectName: ""
  };

  componentDidMount() {
    this.setState({});
  }

  handleProjectInput = e => {
    this.setState({ projectName: e.target.value });
  };

  onAddClicked = async () => {
    let var1 = { projectName: this.state.projectName };
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    let bodyStr = await JSON.stringify(var1);
    let response = await fetch(
      `${URL.SERVER}finline/newProject`,
      {
        method: "POST",
        headers: myHeaders,
        body: bodyStr
      }
    );
    let json = await response.json();
    this.props.snackMsg(json.msg);
  };

  onBackClick = async () => {
    this.props.parentBackClick();
  };

  render() {
    const { projectName } = this.state;

    const fieldsValidity = projectName === undefined || projectName === "";

    const title = `Create Project`;

    return (
      <MuiThemeProvider theme={theme}>
        <Button
          variant="contained"
          color="secondary"
          style={{ marginTop: 20 }}
          onClick={this.onBackClick}
        >
          Back
        </Button>
        <Card style={{ marginTop: "5%" }}>
          <CardHeader title={title} style={{ textAlign: "center" }} />
          <CardContent>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell style={{ border: "none" }}>
                    <TextField
                      placeholder="Project Name"
                      className="textField"
                      value={projectName}
                      onChange={this.handleProjectInput}
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ border: "none", textAlign: "center" }}>
                    <Button
                      variant="contained"
                      color="secondary"
                      style={{ marginTop: 20 }}
                      onClick={this.onAddClicked}
                      disabled={fieldsValidity}
                    >
                      Create
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}
export default CreateProject;
