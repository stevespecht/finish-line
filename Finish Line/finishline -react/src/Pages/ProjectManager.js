//Name: Steven Specht, John Herring
//File: ProjectManager.js
//Date: 29/03/2019
//Purpose: main page for Finishline, select a project and manage all aspects from here

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  FormControl,
  InputLabel,
  MenuItem,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Snackbar
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import LogoComponent from "./LogoComponent";
import TaskManager from "./TaskManager";
import SprintManager from "./SprintManager";
import ProjectOverview from "./ProjectOverview";
import OwnerManager from "./OwnerManager";
import Dashboard from "./Dashboard";
import CreateProject from "./CreateProject";
const URL = require("../url");

class Home extends React.PureComponent {
  state = {
    projects: [],
    selectedProject: "",
    showManagerCard: true,
    showTaskCard: false,
    showSprintCard: false,
    showOverviewCard: false,
    showOwnerCard: false,
    showDashboard: false,
    showCreateProject: false,
    snackbarMsg: ""
  };

  componentDidMount() {
    this.getProjects();
    //this.setState({ projects: ["FinishLine", "GitLove", "Boggle"] });
  }

  getProjects = async () => {
    try {
      let response = await fetch(
        `${URL.SERVER}finline/getProjects`
      );
      let json = await response.json();
      this.setState({ projects: json });
    } catch (err) {
      //console.log(err.message);
    }
  };

  handleProjectSelect = e => {
    this.setState({ selectedProject: e.target.value });
  };

  renderProjectDrop() {
    return this.state.projects.map((project, i) => {
      return (
        <MenuItem key={i} value={project}>
          {project.projectName}
        </MenuItem>
      );
    });
  }

  onTaskManagerClicked = async () => {
    this.setState({
      showManagerCard: false,
      showTaskCard: true,
      showSprintCard: false,
      showOverviewCard: false,
      showOwnerCard: false,
      showDashboard: false,
      showCreateProject: false,
      snackbarMsg: ""
    });
  };

  onSprintManagerClicked = async () => {
    this.setState({
      showManagerCard: false,
      showTaskCard: false,
      showSprintCard: true,
      showOverviewCard: false,
      showOwnerCard: false,
      showDashboard: false,
      showCreateProject: false
    });
  };

  onOverviewClicked = async () => {
    this.setState({
      showManagerCard: false,
      showTaskCard: false,
      showSprintCard: false,
      showOverviewCard: true,
      showOwnerCard: false,
      showDashboard: false,
      showCreateProject: false
    });
  };

  onOwnerClicked = async () => {
    this.setState({
      showManagerCard: false,
      showTaskCard: false,
      showSprintCard: false,
      showOverviewCard: false,
      showOwnerCard: true,
      showDashboard: false,
      showCreateProject: false
    });
  };

  onDashboardClicked = async () => {
    this.setState({
      showManagerCard: false,
      showTaskCard: false,
      showSprintCard: false,
      showOverviewCard: false,
      showOwnerCard: false,
      showDashboard: true,
      showCreateProject: false
    });
  };

  onProjectClicked = async () => {
    this.setState({
      showManagerCard: false,
      showTaskCard: false,
      showSprintCard: false,
      showOverviewCard: false,
      showOwnerCard: false,
      showDashboard: false,
      showCreateProject: true
    });
  };

  onBackClick = async () => {
    this.getProjects();
    this.setState({
      showManagerCard: true,
      showTaskCard: false,
      showSprintCard: false,
      showOverviewCard: false,
      showOwnerCard: false,
      showDashboard: false,
      showCreateProject: false
    });
  };

  //receive snackbar messages from child
  msgFromChild = msg => {
    this.setState({ snackbarMsg: msg, gotData: true });
  };

  snackbarClose = () => {
    this.setState({ gotData: false });
  };

  render() {
    const {
      selectedProject,
      showManagerCard,
      showTaskCard,
      showSprintCard,
      showOverviewCard,
      showOwnerCard,
      showDashboard,
      showCreateProject
    } = this.state;

    const projectValidity =
      selectedProject === undefined || selectedProject === "";

    return (
      <MuiThemeProvider theme={theme}>
        {showManagerCard &&
          !showTaskCard &&
          !showSprintCard &&
          !showOverviewCard &&
          !showOwnerCard &&
          !showDashboard &&
          !showCreateProject && (
            <LogoComponent height="50" width="50" showText={true} />
          )}

        {!showManagerCard && (
          <LogoComponent height="50" width="50" showText={false} />
        )}

        {showManagerCard &&
          !showTaskCard &&
          !showSprintCard &&
          !showOverviewCard &&
          !showOwnerCard &&
          !showDashboard &&
          !showCreateProject && (
            <Card style={{ marginTop: "5%" }}>
              <CardHeader
                title="Project Manager"
                style={{ textAlign: "center" }}
              />
              <CardContent>
                <Table>
                  <TableBody>
                    <TableRow>
                      <TableCell
                        style={{ border: "none", textAlign: "center" }}
                      >
                        <FormControl>
                          <InputLabel htmlFor="Project-Placeholder">
                            Project
                          </InputLabel>
                          <Select
                            value={selectedProject}
                            onChange={this.handleProjectSelect}
                            inputProps={{
                              name: "Project",
                              id: "Project-Placeholder"
                            }}
                            style={{ width: 200 }}
                          >
                            {this.renderProjectDrop()}
                          </Select>
                        </FormControl>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell
                        style={{ border: "none", textAlign: "center" }}
                      >
                        <Button
                          variant="contained"
                          color="secondary"
                          style={{ marginTop: 20, width: 200 }}
                          onClick={this.onProjectClicked}
                        >
                          Create Project
                        </Button>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell
                        style={{ border: "none", textAlign: "center" }}
                      >
                        <Button
                          variant="contained"
                          color="secondary"
                          style={{ marginTop: 20, width: 200 }}
                          onClick={this.onOwnerClicked}
                          disabled={projectValidity}
                        >
                          Owner Manager
                        </Button>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell
                        style={{ border: "none", textAlign: "center" }}
                      >
                        <Button
                          variant="contained"
                          color="secondary"
                          style={{ marginTop: 20, width: 200 }}
                          onClick={this.onTaskManagerClicked}
                          disabled={projectValidity}
                        >
                          Task Manager
                        </Button>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell
                        style={{ border: "none", textAlign: "center" }}
                      >
                        <Button
                          variant="contained"
                          color="secondary"
                          style={{ marginTop: 20, width: 200 }}
                          onClick={this.onSprintManagerClicked}
                          disabled={projectValidity}
                        >
                          Sprint Manager
                        </Button>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell
                        style={{ border: "none", textAlign: "center" }}
                      >
                        <Button
                          variant="contained"
                          color="secondary"
                          style={{ marginTop: 20, width: 200 }}
                          onClick={this.onOverviewClicked}
                          disabled={projectValidity}
                        >
                          Overview
                        </Button>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell
                        style={{ border: "none", textAlign: "center" }}
                      >
                        <Button
                          variant="contained"
                          color="secondary"
                          style={{ marginTop: 20, width: 200 }}
                          onClick={this.onDashboardClicked}
                          disabled={projectValidity}
                        >
                          Dashboard
                        </Button>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </CardContent>
            </Card>
          )}

        {!showManagerCard &&
          !showTaskCard &&
          !showSprintCard &&
          !showOverviewCard &&
          !showOwnerCard &&
          !showDashboard &&
          showCreateProject && (
            <CreateProject
              selectedProject={selectedProject}
              parentBackClick={this.onBackClick}
              snackMsg={this.msgFromChild}
            />
          )}

        {!showManagerCard &&
          showTaskCard &&
          !showSprintCard &&
          !showOverviewCard &&
          !showDashboard &&
          !showCreateProject && (
            <TaskManager
              selectedProject={selectedProject}
              parentBackClick={this.onBackClick}
              snackMsg={this.msgFromChild}
            />
          )}
        {!showManagerCard &&
          !showTaskCard &&
          showSprintCard &&
          !showOverviewCard &&
          !showDashboard &&
          !showCreateProject && (
            <SprintManager
              selectedProject={selectedProject}
              parentBackClick={this.onBackClick}
              snackMsg={this.msgFromChild}
            />
          )}
        {!showManagerCard &&
          !showTaskCard &&
          !showSprintCard &&
          showOverviewCard &&
          !showDashboard &&
          !showCreateProject && (
            <ProjectOverview
              selectedProject={selectedProject}
              parentBackClick={this.onBackClick}
              snackMsg={this.msgFromChild}
            />
          )}

        {!showManagerCard &&
          !showTaskCard &&
          !showSprintCard &&
          !showOverviewCard &&
          showOwnerCard &&
          !showDashboard &&
          !showCreateProject && (
            <OwnerManager
              selectedProject={selectedProject}
              parentBackClick={this.onBackClick}
              snackMsg={this.msgFromChild}
            />
          )}

        {!showManagerCard &&
          !showTaskCard &&
          !showSprintCard &&
          !showOverviewCard &&
          !showOwnerCard &&
          showDashboard &&
          !showCreateProject && (
            <Dashboard
              selectedProject={selectedProject}
              parentBackClick={this.onBackClick}
            />
          )}
        <Snackbar
          open={this.state.gotData}
          message={this.state.snackbarMsg}
          autoHideDuration={4000}
          onClose={this.snackbarClose}
        />
      </MuiThemeProvider>
    );
  }
}
export default Home;
